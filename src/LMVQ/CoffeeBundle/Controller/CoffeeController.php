<?php

namespace LMVQ\CoffeeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class CoffeeController extends Controller
{
    /**
     * @Route("/", name="lmvq_coffee_index")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }

    /**
     * @Route("/programmation", name="lmvq_coffee_programmation")
     * @Template()
     */
    public function programmationAction()
    {
        return array();
    }

    /**
     * @Route("/basket", name="lmvq_coffee_basket")
     * @Template()
     */
    public function basketAction()
    {
        return array();
    }

    /**
     * @Route("/december", name="lmvq_coffee_december")
     * @Template()
     */
    public function decemberAction()
    {
        return array();
    }
}
